#include <thread>
#include <string>
#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <numeric>
#include <atomic>
#include <mutex>

using namespace std;

namespace Atomics
{

	class Data
	{
		vector<int> data_;
		atomic<bool> is_ready_ = false;

	public:
		void read()
		{
			cout << "Start reading in a thread#" << this_thread::get_id() << endl;

			this_thread::sleep_for(2000ms);

			data_.resize(100);

			random_device rd;
			generate(data_.begin(), data_.end(), [&rd] { return rd() % 100; });

			// is_ready_ = true;
			is_ready_.store(true, memory_order_release);  // ---------------------- 

			cout << "End of reading..." << endl;
		}

		void process(int id)
		{
			while (!is_ready_.load(memory_order_acquire)) // -------------------------
			{
			}

			long sum = accumulate(data_.begin(), data_.end(), 0l);

			cout << "Id: " << id << " Sum: " << sum << endl;
		}
	};
}

namespace ConditionVariables
{
	class Data
	{
		vector<int> data_;
		bool is_ready_ = false;
		mutex mtx_is_ready_;
		condition_variable cv_is_ready_;

	public:
		void read()
		{
			cout << "Start reading in a thread#" << this_thread::get_id() << endl;

			this_thread::sleep_for(2000ms);

			data_.resize(100);

			random_device rd;
			generate(data_.begin(), data_.end(), [&rd] { return rd() % 100; });


			///////////////////
			unique_lock<mutex> lk{ mtx_is_ready_ };
			is_ready_ = true;
			lk.unlock();
			///////////////////
			
			cv_is_ready_.notify_all();

			cout << "End of reading..." << endl;
		}

		void process(int id)
		{
			{
				unique_lock<mutex> lk{ mtx_is_ready_ };

				cv_is_ready_.wait(lk, [this] { return is_ready_; });
			}			

			long sum = accumulate(data_.begin(), data_.end(), 0l);

			cout << "Id: " << id << " Sum: " << sum << endl;
		}
	};
}


int main()
{
	using namespace ConditionVariables;

	Data data;

	thread thd1{ [&data] { data.read();  } };

	thread thd2{ [&data] { data.process(1); } };
	thread thd3{ [&data] { data.process(2); } };

	thd1.join();
	thd2.join();
	thd3.join();

	system("PAUSE");
}