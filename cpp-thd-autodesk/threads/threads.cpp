#include <thread>
#include <chrono>
#include <string>
#include <iostream>
#include <vector>

using namespace std;

void hello(chrono::milliseconds time_interval)
{
	string text = "Hello Concurrent World";

	for(const auto& c : text)
	{
		cout << c << " ";
		this_thread::sleep_for(time_interval); // this_thread.sleep_for(chrono::miliseconds(100));
		cout.flush();
	}

	cout << endl;
}

void background_work(int id, const string& text, chrono::milliseconds time_interval)
{
	cout << "bw#" << id << " has started..." << endl;

	for(const auto& c : text)
	{
		cout << "bw#" << id << ": " << c << endl;
		this_thread::sleep_for(time_interval);
	}

	cout << "bw#" << id << " is finished..." << endl;
}

class BackgroundWork
{
	const int id_;
public:
	BackgroundWork(int id)
		: id_{id}
	{}

	void operator()(const string& text, chrono::milliseconds time_interval) const
	{
		cout << "BW#" << id_ << " has started..." << endl;

		for (const auto& c : text)
		{
			cout << "BW#" << id_ << ": " << c << endl;
			this_thread::sleep_for(time_interval);
		}

		cout << "BW#" << id_ << " is finished..." << endl;
	}
};

void copy_data(const vector<int>& source, vector<int>& target)
{
	target.resize(source.size());

	copy(source.begin(), source.end(), target.begin());
}

int main()
{
	thread thd0;

	cout << "thd0.id: " << thd0.get_id() << endl;

	thread thd1(&hello, 100ms);
	thread thd2(&hello, 200ms);

	thd1.join();
	
	cout << "After thd1.join()" << endl;

	thd2.join();	

	cout << "After thd2.join()" << endl;

	system("PAUSE");

	thread thd3(&background_work, 1, "multithreading", 500ms);
	thread thd4(&background_work, 2, "threads", 200ms);

	BackgroundWork bw3(3);
	thread thd5(bw3, "text", 300ms);
	thread thd6(BackgroundWork{ 4 }, "long text", 50ms);
	
	thd3.detach();
	thd4.join();
	thd5.join();
	thd6.join();

	system("PAUSE");

	const vector<int> data = { 1, 2, 4, 5, 646, 234, 5346, 4523, 523, 6346 };

	vector<int> target1;
	vector<int> target2(data.size());
	vector<int> target3;

	thread thd7([&data, &target1] { target1.assign(data.begin(), data.end()); });
	thread thd8([&data, &target2] { copy(data.begin(), data.end(), target2.begin()); });
	thread thd9(&copy_data, std::cref(data), std::ref(target3)); 
	//	thread thd9([&data, &target3] { copy_data(data, target3); });


	thd7.join();
	thd8.join();
	thd9.join();
	
	cout << "target1: ";
	for (int item : target1)
		cout << item << " ";
	cout << endl;

	cout << "target2: ";
	for (int item : target2)
		cout << item << " ";
	cout << endl;

	cout << "target3: ";
	for (int item : target3)
		cout << item << " ";
	cout << endl;

	system("PAUSE");	
}