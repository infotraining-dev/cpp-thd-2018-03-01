#ifndef RAII_THREADS_HPP
#define RAII_THREADS_HPP

#include <thread>
#include <type_traits>

// template variable
template <typename T1, typename T2>
constexpr bool is_similar_v = std::is_same<std::decay_t<T1>, std::decay_t<T2>>::value;

class DetachedThread
{
	std::thread thd_;
public:
	DetachedThread() = default;

	template <
		typename Callable, typename... Args,
		typename = std::enable_if_t<!is_similar_v<Callable, DetachedThread>>
	>
		DetachedThread(Callable&& callable, Args&&... args)
		: thd_{ std::forward<Callable>(callable), std::forward<Args>(args)... }
	{
	}

	std::thread& get()
	{
		return thd_;
	}

	DetachedThread(const DetachedThread&) = delete;
	DetachedThread& operator=(const DetachedThread&) = delete;

	DetachedThread(DetachedThread&&) = default;
	DetachedThread& operator=(DetachedThread&&) = default;

	~DetachedThread()
	{
		if (thd_.joinable())
			thd_.detach();
	}
};

class JoiningThread
{
	std::thread thd_;
public:
	JoiningThread() = default;

	template <
		typename Callable, typename... Args,
		typename = std::enable_if_t<!is_similar_v<Callable, JoiningThread>>
	>
		JoiningThread(Callable&& callable, Args&&... args)
		: thd_{ std::forward<Callable>(callable), std::forward<Args>(args)... }
	{
	}

	std::thread& get()
	{
		return thd_;
	}

	JoiningThread(const JoiningThread&) = delete;
	JoiningThread& operator=(const JoiningThread&) = delete;

	JoiningThread(JoiningThread&&) = default;
	JoiningThread& operator=(JoiningThread&&) = default;

	~JoiningThread()
	{
		if (thd_.joinable())
			thd_.join();
	}
};

#endif
