#include <thread>
#include <string>
#include <iostream>
#include <vector>
#include "raii_threads.hpp"

using namespace std;

void background_work(int id, const string& text, chrono::milliseconds time_interval)
{
	cout << "bw#" << id << " has started..." << endl;

	for (const auto& c : text)
	{
		cout << "bw#" << id << ": " << c << endl;
		this_thread::sleep_for(time_interval);
	}

	cout << "bw#" << id << " is finished..." << endl;
}

int main()
{
	{
		thread thd0{ &background_work, 1, "text", 100ms };
		JoiningThread thd1{ &background_work, 2, "concurrent", 150ms };
		JoiningThread thd2 = move(thd1);
		JoiningThread thd3{ move(thd0) };
		JoiningThread thd4 = move(thd3);

		vector<JoiningThread> threads;

		threads.push_back(move(thd4));
		threads.emplace_back(&background_work, 3, "time", 10ms);
		threads.emplace_back(&background_work, 3, "for", 20ms);
		threads.emplace_back(&background_work, 3, "break", 30ms);
	}

	system("PAUSE");
}