#include <future>
#include <iostream>
#include <string>
#include <iostream>
#include <random>

using namespace std;

int calculate_square(int x)
{
	cout << "Start calculation of square for: " << x << endl;

	if (x == 13)
		throw std::runtime_error("Error#13");

	auto interval = random_device{}() % 3000;

	this_thread::sleep_for(chrono::milliseconds(interval));

	return x * x;
}

void save_to_file(const string& filename, const string& content)
{
	cout << "Starting saving to file" << filename << "... " << endl;
	this_thread::sleep_for(3s);

	cout << content << " saved to " << filename << endl;
}

string download_file(const string& filename)
{
	cout << "Starting downloading a file" << filename << "... " << endl;
	this_thread::sleep_for(3s);

	return "Content of "s + filename;
}

void process_file(shared_future<string> future_content)
{
	string content = future_content.get();

	cout << "Processing of " << content << " in a thread#" << this_thread::get_id() << endl;
}

void may_throw(int x)
{
	if (x % 2 == 0)
		throw std::runtime_error("Error#2");
}

class SquareCalculator
{
	std::promise<int> promise_;
public:
	void operator()(int x)
	{
		auto interval = random_device{}() % 3000;

		this_thread::sleep_for(chrono::milliseconds(interval));

		try
		{
			may_throw(x);
		}
		catch(...)
		{
			exception_ptr excpt = current_exception();
			promise_.set_exception(excpt);			
			return;
		}

		promise_.set_value(x * x);
	}

	future<int> get_future()
	{
		return promise_.get_future();
	}
};

template <typename Task>
auto spawn_task(Task task)
{
	using ResultType = decltype(task());

	packaged_task<ResultType()> pt{ [task] {return task(); } };
	auto future_result = pt.get_future();
	thread thd{ move(pt) };
	thd.detach();

	return future_result;
}

int main()
{
	// 1 -st option - std::packaged_task

	packaged_task<int()> pt1([] { return calculate_square(8); });
	packaged_task<int(int)> pt2(&calculate_square);
	packaged_task<void()> pt3([] { save_to_file("data.txt", "DATA"); });
	packaged_task<string()> pt4([] { return download_file("http://google.com/index.html"); });

	future<int> f1 = pt1.get_future();
	future<int> f2 = pt2.get_future();
	auto f3 = pt3.get_future();
	auto future_content = pt4.get_future();
	auto shared_future_content = future_content.share();

	thread thd1{ move(pt1) };
	thread thd2{ move(pt2), 13 };
	pt3();

	thread thd4{ move(pt4) };
	thd4.detach();

	thread thd5{ &process_file, shared_future_content };
	thread thd6{ &process_file, shared_future_content };

	while (f1.wait_for(200ms) != future_status::ready)
	{
		cout << "I'm waiting for finishing of pt1" << endl;
	}

	cout << "result pt1: " << f1.get() << endl;
	try
	{
		cout << "result pt2:" << f2.get() << endl;
	}
	catch (const runtime_error& e)
	{
		cout << "Caught: " << e.what() << endl;
	}
	f3.wait();

	thd1.join();
	thd2.join();
	thd5.join();
	thd6.join();

	cout << "\n----------------------\n";

	SquareCalculator sq;

	auto f_result = sq.get_future();

	thread thd7{ ref(sq), 12 };
	thd7.detach();

	try
	{
		cout << "Result from SquareCalc: " << f_result.get() << endl;
	}
	catch(const runtime_error& e)
	{
		cout << "Caught: " << e.what() << endl;
	}

	cout << "\n-------------------------";

	auto f_result1 = async(launch::async, &calculate_square, 77);

	{
		vector <future<int>> future_results;

		for (int i = 5; i < 15; ++i)
			future_results.push_back(async(launch::async, &calculate_square, i));

		future_results.push_back(move(f_result1));

		for (auto& f : future_results)
		{
			try
			{
				cout << "Result: " << f.get() << endl;
			}
			catch(const runtime_error& e)
			{
				cout << "Caught an error: " << e.what() << endl;
			}
		}
			

		cout << "END OF SCOPE" << endl;
	}

	cout << "\n-------------------------";

	{
		spawn_task([] { save_to_file("data1.txt", "AAA"); });
		spawn_task([] { save_to_file("data1.txt", "BBB"); });
		auto f_save_finished = spawn_task([] { save_to_file("data1.txt", "BBB"); });
		auto f_result = spawn_task([] { return calculate_square(4); });


		f_save_finished.wait();

		cout << "Result: " << f_result.get() << endl;
	}

	system("PAUSE");
}