#include <chrono>
#include <fstream>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include "active_object.hpp"
#include <oleauto.h>
#include <future>

using namespace std;
		
namespace Before
{
	class Logger
	{
		ofstream fout_;
		mutex mtx_fout_;

	public:
		Logger(const string& file_name)
		{
			fout_.open(file_name);
		}
			
		Logger(const Logger&) = delete;
		Logger& operator=(const Logger&) = delete;

		~Logger()
		{
			fout_.close();
		}

		void log(const string& message)
		{
			lock_guard<mutex> lk{ mtx_fout_ };				

			fout_ << message << endl;
			fout_.flush();
		}
	};
}

namespace After
{
	class Logger
	{
		ofstream fout_;
		ActiveObject ao_;
				
	public:
		Logger(const string& file_name)
		{
			fout_.open(file_name);
		}

		Logger(const Logger&) = delete;
		Logger& operator=(const Logger&) = delete;

		~Logger()
		{
			ao_.send([this] { fout_.close(); });
		}

		void log(const string& message)
		{
			ao_.send([this, message] { do_log(message); });
		}

	private:
		void do_log(const string& message)
		{			
			fout_ << message << endl;
			fout_.flush();
		}
	};
}

//class Document
//{
//	ActiveObject ao_;
//
//public:
//	void save();
//
//	future<void> save_async()
//	{
//		ao_.send([this] { save(); });
//	}
//};

using namespace After;

void run(Logger& logger, int id)
{
	for (int i = 0; i < 10000; ++i)
		logger.log("Log#" + to_string(id) + " - Event#" + to_string(i));
}

int main()
{
	/*
	* Napisz klase Logger, ktora jest thread-safe
	*/

	Logger log("data.log");

	thread thd1(&run, ref(log), 1);
	thread thd2(&run, ref(log), 2);
	thread thd3(&run, ref(log), 3);
	thread thd4(&run, ref(log), 4);

	thd1.join();
	thd2.join();
	thd3.join();
	thd4.join();
}
