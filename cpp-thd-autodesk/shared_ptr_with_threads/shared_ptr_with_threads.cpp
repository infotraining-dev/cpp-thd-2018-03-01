#include <iostream>
#include <string>
#include <thread>
#include "raii_threads.hpp"

using namespace std;

class FileResource
{
	string content_;

public:
	using iterator = string::iterator;
	using const_iterator = string::const_iterator;

	explicit FileResource(const string& content) : content_{content}
	{
		cout << "FileResource(" << content_ << ")" << endl;
	}

	~FileResource()
	{
		cout << "~FileResource(" << content_ << ")" << endl;
	}

	const_iterator begin() const
	{
		return content_.begin();
	}

	const_iterator end() const
	{
		return content_.end();
	}

	iterator begin()
	{
		return content_.begin();
	}

	iterator end()
	{
		return content_.end();
	}
};

void read_from_file(const FileResource& f)
{
	cout << "Reading from file in THD#" << this_thread::get_id() << endl;

	for(const auto& c : f)
	{
		cout << c << " ";
		this_thread::sleep_for(50ms);
		cout.flush();
	}

	cout << "End reading of file in thd#" << this_thread::get_id() << endl;
}

JoiningThread create_thread_reading_file()
{
	auto file = make_shared<const FileResource>("another file");

	return JoiningThread{ [file] { read_from_file(*file); } };
}

int main()
{
	{	
		FileResource file("content of file");
		JoiningThread thd1, thd2, thd3;				
		{			
			thd1 = JoiningThread{ &read_from_file, cref(file) };
			thd2 = JoiningThread{ [&file] { read_from_file(file); } };
			thd3 = create_thread_reading_file();
		}
	}

	system("PAUSE");
}
