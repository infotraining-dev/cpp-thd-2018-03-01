#include <iostream>
#include <thread>
#include <mutex>

class BankAccount
{
    const int id_;
    double balance_;
	mutable std::recursive_mutex mtx_;

public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {		
		double local_balance{};
		{
			std::lock_guard<std::recursive_mutex> lk{ mtx_ };
			local_balance = balance_;
		}

		std::cout << "Bank Account #" << id_ << "; Balance = " << local_balance << std::endl;
    }

	void print_alt() const
	{		
		std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
	}

    void transfer(BankAccount& to, double amount)
    {
		std::unique_lock<std::recursive_mutex> lk_from{ mtx_, std::defer_lock };
		std::unique_lock<std::recursive_mutex> lk_to{ to.mtx_, std::defer_lock };

		std::lock(lk_from, lk_to);

        balance_ -= amount;
        to.balance_ += amount;
    }

	void transfer_alt(BankAccount& to, double amount)
	{
		withdraw(amount);
		to.deposit(amount);
	}

    void withdraw(double amount)
    {
		std::lock_guard<std::recursive_mutex> lk{ mtx_ };

        balance_ -= amount;
    }

    void deposit(double amount)
    {
		std::lock_guard<std::recursive_mutex> lk{ mtx_ };

        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
		std::lock_guard<std::recursive_mutex> lk{ mtx_ };

        return balance_;
    }

	void lock()
    {
		mtx_.lock();
    }

	void unlock()
    {
		mtx_.unlock();
    }

	std::unique_lock<std::recursive_mutex> begin_transaction()
    {
		std::unique_lock<std::recursive_mutex> lk{ mtx_ };
		return lk;
    }
};

void make_deposits(BankAccount& account, int no_of_operations, double amount)
{
	for (int i = 0; i < no_of_operations; ++i)
		account.deposit(amount);
}

void make_withdraws(BankAccount& account, int no_of_operations, double amount)
{
	for (int i = 0; i < no_of_operations; ++i)
		account.withdraw(amount);
}

void make_transfers(BankAccount& from, BankAccount& to, int no_of_operations, double amount)
{
	for (int i = 0; i < no_of_operations; ++i)
		from.transfer(to, amount);
}

int main()
{
    BankAccount ba1(1, 1'000'000);
    BankAccount ba2(2, 1'000'000);

    ba1.print();
    ba2.print();

	std::cout << "\n---------------------" << std::endl;

	std::thread thd1{ &make_deposits, std::ref(ba1), 1'000'000, 1.0 };
	std::thread thd2{ &make_withdraws, std::ref(ba1), 1'000'000, 1.0 };
	std::thread thd3{ &make_transfers, std::ref(ba1), std::ref(ba2), 1'000'000, 1.0 };
	std::thread thd4{ &make_transfers, std::ref(ba2), std::ref(ba1), 1'000'000, 1.0 };


	// Transaction 1
	{
		std::lock_guard<BankAccount> lk_transaction{ ba1 };
		ba1.deposit(1000.0);
		ba1.withdraw(500.0);
	}
	

	// Transaction 2
	{
		auto transaction_scope = ba1.begin_transaction();
		ba1.deposit(1000.0);
		ba1.withdraw(500.0);
	}

	thd1.join();
	thd2.join();
	thd3.join();
	thd4.join();

	ba1.print();
	ba2.print();

    system("PAUSE");
}
