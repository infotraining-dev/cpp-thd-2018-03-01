#include <thread>
#include <mutex>
#include <iostream>

using namespace std;

timed_mutex mtx;

void background_work(int id, chrono::milliseconds timeout)
{
	cout << "THD#" << id << " is waiting for a mutex..." << endl;

	unique_lock<timed_mutex> lk_mtx(mtx, try_to_lock);

	if (!lk_mtx.owns_lock())
	{
		do
		{
			cout << "THD#" << id << " doesn't own a lock..." << " Waits for a mutex..." << endl;
		} while (!lk_mtx.try_lock_for(timeout));

	}

	cout << "THD#" << id << " has started a job" << endl;

	this_thread::sleep_for(5000ms);

	cout << "THD#" << id << "ends work..." << endl;
}

int main()
{
	thread thd1{ &background_work, 1, 500ms };
	thread thd2{ &background_work, 2, 300ms };

	thd1.join();
	thd2.join();

	system("PAUSE");
}

