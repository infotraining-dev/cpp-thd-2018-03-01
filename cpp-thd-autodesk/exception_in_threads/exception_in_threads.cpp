#include <thread>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void may_throw(int id, int arg)
{
	if (arg == 13)
		throw runtime_error("Error#13 from THD#" + to_string(id));

	if (id == 2)
		throw invalid_argument("Error#2 from THD#" + to_string(id));
}

template <typename Callable>
void thread_exception_wrapper(Callable f, exception_ptr& excpt)
{
	try
	{
		f();
	}
	catch (...)
	{
		excpt = current_exception();
	}
}

void background_work(int id, size_t count)
{
	for (size_t i = 0; i < count; ++i)
	{
		cout << "THD#" << id << ": " << i << endl;
		this_thread::sleep_for(100ms);
		may_throw(id, i);
	}
}

int main()
{	
	try
	{
		exception_ptr excpt1;
		thread thd1([&] { thread_exception_wrapper([&] { background_work(1, 15); }, excpt1); });
		thd1.join();

		if (excpt1)
		{
			rethrow_exception(excpt1);
		}
	}
	catch(const runtime_error& e)
	{
		cout << "Caught: " << e.what() << endl;
	}

	const auto cores_no = max(thread::hardware_concurrency(), 1u);

	vector<tuple<thread, exception_ptr>> threads(cores_no);	

	for(auto i = 0u; i < cores_no; ++i)
	{
		get<thread>(threads[i]) = 
			thread{ [&] { thread_exception_wrapper([&] { background_work(i + 1, 10 * (i + 1)); }, get<exception_ptr>(threads[i])); } };
	}

	for (auto i = 0u; i < cores_no; ++i)
	{
		get<thread>(threads[i]).join();
	}

	for(auto& thd : threads)
	{
		try
		{
			auto&& excpt = get<exception_ptr>(thd);

			if (excpt)
				rethrow_exception(excpt);
		}
		catch(const runtime_error& e)
		{
			cout << "Caught: " << e.what() << endl;
		}
		catch(const invalid_argument& e)
		{
			cout << "Caught invalid arg exception: " << e.what() << endl;
		}
	}

	system("PAUSE");
}