#include <thread>
#include <string>
#include <chrono>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void background_work(int id, const string& text, chrono::milliseconds time_interval)
{
	cout << "bw#" << id << " has started..." << endl;

	for (const auto& c : text)
	{
		cout << "bw#" << id << ": " << c << endl;
		this_thread::sleep_for(time_interval);
	}

	cout << "bw#" << id << " is finished..." << endl;
}

std::thread create_thread(const string& text)
{
	static int id = 0;
	return thread{ &background_work, ++id, text, 100ms };
}

int main()
{
	auto cores_no = max(thread::hardware_concurrency(), 1u);
	cout << "Hardware concurrency (cores no): " << cores_no << endl;

	thread thd1 = create_thread("text");

	thread thd2 = move(thd1);

	vector<thread> threads(4);

	threads[0] = move(thd2);
	threads[1] = create_thread("hello");
	threads[2] = create_thread("thread");
	threads[3] = create_thread("concurrent");

	threads.push_back(create_thread("new thread"));
	threads.emplace_back(&background_work, 665, "from emplace back", 10ms);	

	for (auto& thd : threads)
		thd.join();

	system("PAUSE");
}