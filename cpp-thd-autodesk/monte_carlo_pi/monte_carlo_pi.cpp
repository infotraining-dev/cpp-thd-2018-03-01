#include <iostream>
#include <thread>
#include <random>
#include <algorithm>
#include <numeric>
#include <atomic>
#include <mutex>
#include <future>

using namespace std;

const int N = 100'000'000;

class RandomRealDisitribuiton
{
	mt19937_64 rnd_gen_;
	uniform_real_distribution<double> rnd_distr_;
public:
	RandomRealDisitribuiton(double min, double max) : rnd_gen_{ random_device{}() }, rnd_distr_{min, max}
	{		
	}

	double operator()()
	{
		return rnd_distr_(rnd_gen_);
	}
};

int calc_counts(int n)
{
	RandomRealDisitribuiton rnd_distr{ -1, 1 };

	int count{};

	for (int i = 0; i < n; ++i)
	{
		auto x = rnd_distr();
		auto y = rnd_distr();
		auto z = x*x + y*y;
		if (z <= 1)
			++count;
	}

	return count;
}

void calc_counts(int n, int& count)
{
	RandomRealDisitribuiton rnd_distr{ -1, 1 };

	for (int i = 0; i < n; ++i)
	{
		auto x = rnd_distr();
		auto y = rnd_distr();
		auto z = x*x + y*y;
		if (z <= 1)
			++count;
	}
}

struct Counter
{
	int counter;
	mutex mtx_counter;
};

void calc_counts(int n, Counter& count)
{
	RandomRealDisitribuiton rnd_distr{ -1, 1 };

	int local_counter{};
	for (int i = 0; i < n; ++i)
	{
		auto x = rnd_distr();
		auto y = rnd_distr();
		auto z = x*x + y*y;
		if (z <= 1)
		{
			++local_counter;
		}
	}

	{		
		lock_guard<mutex> lk_counter{ count.mtx_counter };
		count.counter += local_counter;
	}

	//...
}

void calc_counts(int n, atomic<int>& count)
{
	RandomRealDisitribuiton rnd_distr{ -1, 1 };

	for (int i = 0; i < n; ++i)
	{
		auto x = rnd_distr();
		auto y = rnd_distr();
		auto z = x*x + y*y;
		if (z <= 1)
		{			
			++count;			
		}
	}
}


void calc_counts_with_local_counter(int n, int& count)
{
	RandomRealDisitribuiton rnd_distr{ -1, 1 };

	int local_count{};
	for (int i = 0; i < n; ++i)
	{
		auto x = rnd_distr();
		auto y = rnd_distr();
		auto z = x*x + y*y;
		if (z <= 1)
			++local_count;
	}

	count += local_count;
}

void single_thread_pi()
{
	const auto t1 = chrono::high_resolution_clock::now();

	int count{};

	calc_counts(N, count);

	const double pi = double(count) / N * 4.0;

	const auto t2 = chrono::high_resolution_clock::now();
	const auto time_elapsed = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();

	cout << "Pi (ST): " << pi << " - " << time_elapsed << "ms" << endl;
}

void multi_thread_pi_no_false_sharing()
{
	const auto t1 = chrono::high_resolution_clock::now();

	const auto cores_no = max(thread::hardware_concurrency(), 1u);
	const auto n_per_thread = N / cores_no;

	vector<int> partial_counts(cores_no);	
	vector<thread> threads(cores_no);

	for(auto i = 0u; i < cores_no; ++i)
		threads[i] = thread{ [&partial_counts, n_per_thread, i] { calc_counts_with_local_counter(n_per_thread, partial_counts[i]); } };
	
	for (auto& thd : threads)
		thd.join();

	auto total_count = accumulate(partial_counts.begin(), partial_counts.end(), 0.0);
	
	const double pi = double(total_count) / N * 4.0;

	const auto t2 = chrono::high_resolution_clock::now();
	const auto time_elapsed = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();

	cout << "Pi (MT NFS): " << pi << " - " << time_elapsed << "ms" << endl;
}

void multi_thread_pi_false_sharing()
{
	auto t1 = chrono::high_resolution_clock::now();

	const auto cores_no = max(thread::hardware_concurrency(), 1u);
	const auto n_per_thread = N / cores_no;

	vector<int> partial_counts(cores_no);
	vector<thread> threads(cores_no);

	for (auto i = 0u; i < cores_no; ++i)
	{
		threads[i] = thread{ [&partial_counts, n_per_thread, i] { calc_counts(n_per_thread, partial_counts[i]); } };
	}

	for (auto& thd : threads)
		thd.join();

	const auto total_count = accumulate(partial_counts.begin(), partial_counts.end(), 0.0);

	const double pi = double(total_count) / N * 4.0;

	const auto t2 = chrono::high_resolution_clock::now();
	const auto time_elapsed = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();

	cout << "Pi (MT FS): " << pi << " - " << time_elapsed << "ms" << endl;
}

void multi_thread_pi_with_atomic()
{
	const auto t1 = chrono::high_resolution_clock::now();

	const auto cores_no = max(thread::hardware_concurrency(), 1u);
	const auto n_per_thread = N / cores_no;

	atomic<int> count{};

	vector<thread> threads(cores_no);

	for (auto i = 0u; i < cores_no; ++i)
	{
		threads[i] = thread{ [&count, n_per_thread] { calc_counts(n_per_thread, count); } };
	}

	for (auto& thd : threads)
		thd.join();

	const double pi = double(count) / N * 4.0;

	const auto t2 = chrono::high_resolution_clock::now();
	const auto time_elapsed = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();

	cout << "Pi (MT ATOMIC): " << pi << " - " << time_elapsed << "ms" << endl;
}

void multi_thread_pi_with_mutex()
{
	const auto t1 = chrono::high_resolution_clock::now();

	const auto cores_no = max(thread::hardware_concurrency(), 1u);
	const auto n_per_thread = N / cores_no;

	Counter count;

	vector<thread> threads(cores_no);

	for (auto i = 0u; i < cores_no; ++i)
	{
		threads[i] = thread{ [n_per_thread, &count] { calc_counts(n_per_thread, count); } };
	}

	for (auto& thd : threads)
		thd.join();

	const double pi = double(count.counter) / N * 4.0;

	const auto t2 = chrono::high_resolution_clock::now();
	const auto time_elapsed = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();

	cout << "Pi (MT MUTEX): " << pi << " - " << time_elapsed << "ms" << endl;
}

void multi_thread_pi_with_futures()
{
	const auto t1 = chrono::high_resolution_clock::now();

	const auto cores_no = max(thread::hardware_concurrency(), 1u);
	const auto n_per_thread = N / cores_no;

	vector<future<int>> partial_results;
	partial_results.reserve(cores_no);

	for (auto i = 0u; i < cores_no; ++i)
		partial_results.push_back(async(launch::async, [n_per_thread] { return calc_counts(n_per_thread); }));

	int total_counts{};

	for (auto& pr : partial_results)
		total_counts += pr.get();

	const double pi = double(total_counts) / N * 4.0;

	const auto t2 = chrono::high_resolution_clock::now();
	const auto time_elapsed = chrono::duration_cast<chrono::milliseconds>(t2 - t1).count();

	cout << "Pi (MT FUTURES): " << pi << " - " << time_elapsed << "ms" << endl;
}


int main()
{
	single_thread_pi();

	cout << "\n\n";

	multi_thread_pi_false_sharing();

	cout << "\n\n";

	multi_thread_pi_no_false_sharing();

	cout << "\n\n";

	multi_thread_pi_with_atomic();

	cout << "\n\n";

	multi_thread_pi_with_mutex();

	cout << "\n\n";
	
	multi_thread_pi_with_futures();

	system("PAUSE");
}