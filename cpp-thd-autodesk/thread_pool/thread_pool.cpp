#include <iostream>
#include <thread>
#include "thread_safe_queue.hpp"
#include <random>
#include <atomic>
#include <string>
#include <future>

using namespace std;


using Task = std::function<void()>;

/***
 * https://www.youtube.com/watch?v=QIHy8pXbneI - effective implementation of thread pool
 */

class ThreadPool 
{
	ThreadSafeQueue<Task> task_q_;
	std::vector<thread> threads_;
	static const nullptr_t end_of_work_;
public:
	explicit ThreadPool(size_t no_of_threads)
	{
		for (size_t i = 0; i < no_of_threads; ++i)
			threads_.emplace_back([this] { run(); });
	}

	ThreadPool(const ThreadPool&) = delete;
	ThreadPool& operator=(const ThreadPool&) = delete;

	~ThreadPool()
	{
		for (size_t i = 0; i < threads_.size(); ++i)
			task_q_.push(end_of_work_);

		for (auto& thd : threads_)
			thd.join();
	}

	/*void submit(Task task)
	{
		if (!task)
			throw std::invalid_argument("Empty function");

		task_q_.push(task);
	}*/

	template <typename Task>
	auto submit(Task&& task) -> future<decltype(task())>
	{
		using ReturnType = decltype(task());

		auto pt = make_shared<packaged_task<ReturnType()>>(std::forward<Task>(task));
		future<ReturnType> fresult = pt->get_future();
		task_q_.push([pt] { (*pt)(); });

		return fresult;
	}


private:
	void run()
	{
		while (true) 
		{
			Task task;
			task_q_.pop(task);

			if (task == end_of_work_)
				return;

			task();  // execution of task		
		}
	}
};

const nullptr_t ThreadPool::end_of_work_ = nullptr;

void background_work(int id)
{
	cout << "BW#" << id << "has started..." << endl;

	auto interval = random_device{}() % 1000;

	this_thread::sleep_for(chrono::milliseconds(interval));

	cout << "BW#" << id << "has finished..." << endl;
}

void save_to_file(const string& filename, const string& content)
{
	cout << "Starting saving to file" << filename << "... " << endl;
	this_thread::sleep_for(3s);

	cout << content << " saved to " << filename << endl;
}

string download_file(const string& filename)
{
	cout << "Starting downloading a file" << filename << "... " << endl;
	this_thread::sleep_for(3s);

	return "Content of "s + filename;
}


int main()
{
	{
		ThreadPool thread_pool(max(1u, thread::hardware_concurrency()));

		thread_pool.submit([] { background_work(1); });
		thread_pool.submit([] { background_work(2); });
		thread_pool.submit([] { background_work(3); });

		for (int i = 4; i <= 32; ++i)
			thread_pool.submit([i] { background_work(i); });
	} // joining working threads in the pool destructor

	cout << "\n\n";

	{
		ThreadPool thread_pool{ 4 };

		thread_pool.submit([] { save_to_file("data1.txt", "AAA"); });
		thread_pool.submit([] { save_to_file("data2.txt", "BBB"); });

		vector<string> urls = { "a", "b", "c", "d", "e", "f" };

		vector<future<string>> contents;

		for(const auto& url : urls)
		{
			contents.push_back(thread_pool.submit([url] { return download_file("http://google.com/"s + url); }));
		}

		cout << "\n\n"; 
		for(auto& c : contents)
		{
			cout << c.get() << endl;
		}
	}

	system("PAUSE");
}