#include <algorithm>
#include <chrono>
#include <execution>
#include <iostream>
#include <numeric>
#include <random>
#include <string>
#include <filesystem>
#include <map>

using namespace std;

template <typename F>
class Stoper
{
    std::string description_;
    std::chrono::high_resolution_clock::time_point start_;
    F func_;

public:
    Stoper(const std::string& description, F func) : description_{description}, func_{func}
    {
        start_ = std::chrono::high_resolution_clock::now();
        func_();
    }

    Stoper(const Stoper&) = delete;
    Stoper& operator=(const Stoper&) = delete;

    Stoper(Stoper&&) = default;
    Stoper& operator=(Stoper&&) = default;

    ~Stoper()
    {
        auto end = std::chrono::high_resolution_clock::now();

        auto time_interval = std::chrono::duration_cast<std::chrono::milliseconds>(end - start_);

        std::cout << description_ << " - time: " << time_interval.count() << "ms" << std::endl;
    }
};

template <typename F>
auto BENCHMARK(const std::string& description, F&& func)
{
    return Stoper<F>(description, std::forward<F>(func));
}

namespace fs = std::experimental::filesystem;

int main()
{
    vector<int> vec(100'000'000);

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<int> rnd_distr(0, 100);

    generate(vec.begin(), vec.end(), [&] { return rnd_distr(rnd_gen); });

    long long sum1{};

    BENCHMARK("normal", [&]() {
        sum1 = accumulate(vec.begin(), vec.end(), 0LL);
    });

    std::cout << "sum1: " << sum1 << endl;

    long long sum2{};

    BENCHMARK("parallel", [&]() {
        sum2 = reduce(std::execution::par_unseq, vec.begin(), vec.end(), 0LL);
    });

    std::cout << "sum2: " << sum2 << endl;

    cout << "\n---------------------------------------\n";

	fs::path user_path = R"(C:\Users\Krystian)";
	vector<fs::directory_entry> dir_entries(fs::recursive_directory_iterator(user_path), {});


    uint64_t total_size1{};
	

	BENCHMARK("size of directory", [&] {
		total_size1 = accumulate(dir_entries.begin(), dir_entries.end(), 0ULL, 
			[](const auto& ts, const fs::directory_entry& de) {
			auto fsize = fs::is_regular_file(de) ? fs::file_size(de.path()) : 0u;
				return ts + fsize;
		});			
	});

	std::cout << "total_size1: " << total_size1 << endl;

	uint64_t total_size2{};

	BENCHMARK("size of directory - parallel", [&] {

		vector<uint64_t> file_sizes(dir_entries.size());

		transform(std::execution::par, dir_entries.begin(), dir_entries.end(), file_sizes.begin(),
			[](const fs::directory_entry& de) { return fs::is_regular_file(de) ? fs::file_size(de.path()) : 0u; });

		total_size2 = reduce(std::execution::par_unseq, file_sizes.begin(), file_sizes.end(), 0ULL);
	});

	std::cout << "total_size2: " << total_size1 << endl;

    std::system("PAUSE");
}

